##  Spark 建立 Titanic 机器学习模型

## 数据源
- Kaggle 项目网址 https://www.kaggle.com/c/titanic/overview
- Titanic 数据 s3://dywx-recommend/data/test-data/zhouwei/dataset/spark/titanic

## 主要内容
- 使用 Spark 进行特征清洗、处理，具体参见 FeatureProcessor.java；
- 在特征数据上进行模型训练，主要包括两个模型  Logistic Regression 和 XGBoost, 具体参见 Models.java：
- 构建机器学习的 Pipeline，具体参见 Pipeline.java；

## 补充
-  Logistic Regression 是 Spark 内建的机器学习模型，一般常作为机器学习的 Baseline，特点简单易用，解释性强。XGBoost 是业界常用的机器学习模型，在 Kaggle 等比赛使用广泛。注意：在 Spark 上运行 XGBoost 需要 Spark 的版本在 2.4.3 版本以上。
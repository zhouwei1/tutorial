import static org.apache.spark.sql.functions.col;

import ml.dmlc.xgboost4j.scala.spark.XGBoostClassificationModel;
import ml.dmlc.xgboost4j.scala.spark.XGBoostClassifier;
import org.apache.spark.ml.classification.LogisticRegression;
import org.apache.spark.ml.classification.LogisticRegressionModel;
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator;
import org.apache.spark.ml.param.ParamMap;
import org.apache.spark.ml.tuning.ParamGridBuilder;
import org.apache.spark.ml.tuning.TrainValidationSplit;
import org.apache.spark.ml.tuning.TrainValidationSplitModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;


public class Pipeline {

  private static final String APP_NAME = "TITANIC";

  public static void main(String[] args) {
    SparkSession ss = SparkSession.builder().appName(APP_NAME).enableHiveSupport().getOrCreate();

    /* load data from path */
//    String trainPath = "your_train_data_path";
    String trainPath = "s3://dywx-recommend/data/test-data/zhouwei/dataset/spark/titanic/train.csv";
    Dataset<Row> trainData = loadRawData(ss, Constants.TRAIN_DATA_FORMAT, trainPath);

    /* transform raw data to feature data*/
    Dataset<Row> trainFeatureData = transformFeatures(ss, trainData);

    /* train model */
    trainLrModel(trainFeatureData);

    trainXgbModel(trainFeatureData);
  }

  private static void trainLrModel(Dataset<Row> data) {
    Dataset<Row>[] parts = data.randomSplit(new double[]{0.8, 0.2}, 0);
    Dataset<Row> trainData = parts[0];
    Dataset<Row> testData = parts[1];

    MulticlassClassificationEvaluator evaluator = getBinaryEvaluator();

    LogisticRegression lr = Models.creatLrClassifier();
    LogisticRegressionModel model = lr.fit(trainData);

    Dataset<Row> result = model.transform(testData);
    double accuracy = evaluator.evaluate(result);
    System.out.format("Logistic Regression Accuracy: %f", accuracy);
  }

  private static void trainXgbModel(Dataset<Row> data) {
    Dataset<Row>[] parts = data.drop("PassengerId").randomSplit(new double[]{0.8, 0.2}, 0);
    Dataset<Row> trainData = parts[0];
    Dataset<Row> testData = parts[1];

    XGBoostClassifier xgb = Models.createXgbClassifier();
    XGBoostClassificationModel model = xgb.fit(trainData);

    MulticlassClassificationEvaluator evaluator = getBinaryEvaluator();

    Dataset<Row> result = model.transform(testData);
    double accuracy = evaluator.evaluate(result);
    System.out.format("Xgboost Accuracy: %f", accuracy);
  }

  private static void girdSearchModelParameter(Dataset<Row> data) {
    LogisticRegression lr = Models.creatLrClassifier();
    ParamMap[] paramGrid = new ParamGridBuilder()
        .addGrid(lr.regParam(), new double[]{0.1, 0.2})
        .addGrid(lr.elasticNetParam(), new double[] {0.5, 0.6, 0.8})
        .build();

    MulticlassClassificationEvaluator evaluator = new MulticlassClassificationEvaluator()
        .setLabelCol("label")
        .setPredictionCol("prediction")
        .setMetricName("accuracy");
    TrainValidationSplit trainValidSplit = new TrainValidationSplit()
        .setEstimator(lr)
        .setEvaluator(evaluator)
        .setEstimatorParamMaps(paramGrid)
        .setTrainRatio(0.8);

    TrainValidationSplitModel splitModel = trainValidSplit.fit(data);

    LogisticRegressionModel model = (LogisticRegressionModel) splitModel.bestModel();
    double alpha = model.getRegParam();
    double beta = model.getElasticNetParam();
    System.out.format("Alpha: %f, Beta: %f", alpha, beta);
  }

  private static MulticlassClassificationEvaluator getBinaryEvaluator() {
    MulticlassClassificationEvaluator evaluator = new MulticlassClassificationEvaluator()
        .setLabelCol("label")
        .setPredictionCol("prediction")
        .setMetricName("accuracy");
    return evaluator;
  }

  private static Dataset<Row> transformFeatures(SparkSession ss, Dataset<Row> data) {
    /* extract title from name */
    Dataset<Row> titleData = FeatureProcessor.extractTitle(ss, data);

    /* fill null value in age*/
    Integer defaultAge = 0;
    Dataset<Row> ageData = FeatureProcessor.fillValue(titleData, "Age", defaultAge);

    /* count family member */
    Dataset<Row> familyData = FeatureProcessor.countFamily(ageData);

    /* average fare of group ticket*/
    Dataset<Row> fareData = FeatureProcessor.computeAverageFare(familyData);

    /* fill mode value in Embarked*/
    Dataset<Row> embarkData = FeatureProcessor.fillValue(fareData, "Embarked", "S");

    /* transform string value to categorical value */
    String[] indexColumns = {"Title", "Sex", "Embarked"};
    String indexPrefix = "Index";
    Dataset<Row> indexData = FeatureProcessor.stringIndices(embarkData, indexColumns, indexPrefix);

    /* encode categorical value to one-hot value */
    String[] encColumns = {"Index_Title", "Index_Sex", "Index_Embarked", "Ticket_Count"};
    String encPrefix = "Enc";
    Dataset<Row> encData = FeatureProcessor.encodeByOneHot(indexData, encColumns, encPrefix);

    Dataset<Row> reduceData = encData.select(col("PassengerId"),
        col("Pclass").cast(DataTypes.DoubleType),
        col("Enc_Index_Title"),
        col("Enc_Index_Sex"),
        col("Fill_Age").cast(DataTypes.DoubleType),
        col("SibSp").cast(DataTypes.DoubleType),
        col("Parch").cast(DataTypes.DoubleType),
        col("Family").cast(DataTypes.DoubleType),
        col("Fare"),
        col("Avg_Fare"),
        col("Enc_Index_Embarked"),
        col("Enc_Ticket_Count"),
        col("Survived").as("label"));

    String[] featureColumns = new String[] {"Pclass", "Enc_Index_Title", "Enc_Index_Sex",
        "Fill_Age", "SibSp", "Parch", "Family", "Fare", "Avg_Fare", "Enc_Index_Embarked",
        "Enc_Ticket_Count"};
    Dataset<Row> featureData = FeatureProcessor.assemblerFeature(reduceData, featureColumns)
        .select("PassengerId", "features", "label");

    return featureData;
  }

  private static Dataset<Row> loadRawData(SparkSession ss, Integer type, String path) {
    StructType schema = getDataSchema(type);
    Dataset<Row> data = ss.read().schema(schema).option("header", true).csv(path);
    return data;
  }

  private static StructType getDataSchema(Integer type) {
    StructType schema = new StructType().add("PassengerId", DataTypes.IntegerType, false);
    if (type.equals(Constants.TRAIN_DATA_FORMAT)) {
      schema = schema.add("Survived", DataTypes.IntegerType, false);
    }
    schema = schema.add("Pclass", DataTypes.IntegerType, true)
        .add("Name", DataTypes.StringType, true)
        .add("Sex", DataTypes.StringType, true)
        .add("Age", DataTypes.IntegerType, true)
        .add("SibSp", DataTypes.IntegerType, true)
        .add("Parch", DataTypes.IntegerType, true)
        .add("Ticket", DataTypes.StringType, true)
        .add("Fare", DataTypes.DoubleType, true)
        .add("Cabin", DataTypes.StringType, true)
        .add("Embarked", DataTypes.StringType, true);
    return schema;
  }

}

import ml.dmlc.xgboost4j.scala.spark.XGBoostClassificationModel;
import ml.dmlc.xgboost4j.scala.spark.XGBoostClassifier;
import org.apache.spark.ml.classification.LogisticRegression;
import org.apache.spark.ml.classification.LogisticRegressionModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public class Models {

  public static LogisticRegression creatLrClassifier() {
    LogisticRegression lr = new LogisticRegression()
        .setFeaturesCol("features")
        .setLabelCol("label")
        .setMaxIter(2000)
        .setRegParam(0.1)
        .setElasticNetParam(0.5);
    return lr;
  }

  public static XGBoostClassifier createXgbClassifier() {
    XGBoostClassifier xgbClassifier = new XGBoostClassifier()
        .setFeaturesCol("features")
        .setLabelCol("label")
        .setObjective("binary:logistic")
        .setNumRound(100)
        .setEta(0.1)
        .setMaxDepth(15)
        .setEvalMetric("logloss")
        .setAlpha(0.2)
        .setLambda(0.2)
        .setSubsample(0.8)
        .setColsampleBytree(0.7)
        .setMinChildWeight(30.0)
        .setMissing(0.0F)
        .setSilent(1);
//    XGBoostClassificationModel xgbModel = xgbClassifier.fit(input);
    return xgbClassifier;
  }

}

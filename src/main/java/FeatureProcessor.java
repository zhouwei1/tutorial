import static org.apache.spark.sql.functions.callUDF;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.count;
import static org.apache.spark.sql.functions.when;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.ml.feature.OneHotEncoderEstimator;
import org.apache.spark.ml.feature.OneHotEncoderModel;
import org.apache.spark.ml.feature.StringIndexer;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.types.DataTypes;

public class FeatureProcessor {

  private static final Pattern TITLE_PATTERN = Pattern.compile(" ([A-Za-z]+)\\.");

  public static Dataset<Row> extractTitle(SparkSession ss, Dataset<Row> input) {
    String functionName = "extract_title";
    UDF1<String, String> mapUdf = new UDF1<String, String>() {
      @Override
      public String call(String name) throws Exception {
        if (StringUtils.isNotEmpty(name)) {
          Matcher matcher = TITLE_PATTERN.matcher(name);
          if (matcher.find()) {
            String appellation = matcher.group(1);
            String title = Constants.TITLE_MAP.get(appellation);
            if (StringUtils.isNotEmpty(title)) {
              return title;
            }
          }
        }
        return Constants.DEFAULT_TITLE;
      }
    };
    ss.udf().register(functionName, mapUdf, DataTypes.StringType);
    Dataset<Row> output = input.withColumn("Title",
        callUDF(functionName, input.col("Name")));
    return output;
  }

  public static Dataset<Row> countFamily(Dataset<Row> input) {
    Dataset<Row> output = input.withColumn("Family",
        input.col("SibSp").$plus(input.col("Parch")));
    return output;
  }

  public static Dataset<Row> stringIndices(Dataset<Row> input, String[] columns, String prefix) {
    for (int i = 0; i < columns.length; i++) {
      String column = columns[i];
      String indexedColumn = String.format("%s_%s", prefix, column);
      StringIndexer indexer = new StringIndexer().setInputCol(column).setOutputCol(indexedColumn)
          .setHandleInvalid("keep");
      input = indexer.fit(input).transform(input);
    }
    return input;
  }

  public static Dataset<Row> encodeByOneHot(Dataset<Row> input, String[] columns, String prefix) {
    String[] encColumns = new String[columns.length];
    for (int i = 0; i < columns.length; i++) {
      String column = columns[i];
      String newColumn = String.format("%s_%s", prefix, column);
      encColumns[i] = newColumn;
    }

    OneHotEncoderEstimator encoder = new OneHotEncoderEstimator().setInputCols(columns)
        .setOutputCols(encColumns);
    OneHotEncoderModel model = encoder.fit(input);
    Dataset<Row> output = model.transform(input);
    return output;
  }

  public static Dataset<Row> computeAverageFare(Dataset<Row> input) {
    Dataset<Row> groupTicketDs = input.groupBy(col("Ticket"))
        .agg(count("Ticket").as("Ticket_Count"))
        .select(col("Ticket"), col("Ticket_Count").cast(DataTypes.DoubleType));
    Dataset<Row> mergeDs = input.join(groupTicketDs,
        input.col("Ticket").equalTo(groupTicketDs.col("Ticket")))
        .drop(groupTicketDs.col("Ticket"));
    mergeDs = mergeDs.withColumn("Avg_Fare", col("Fare").$div(col("Ticket_Count")));
    return mergeDs;
  }


  public static Dataset<Row> fillValue(Dataset<Row> input, String column, Object mode) {
    String fillColumn = String.format("Fill_%s", column);
    Dataset<Row> output = input.withColumn(fillColumn, when(col(column).isNotNull(), col(column))
        .otherwise(mode));
    return output;
  }

  public static Dataset<Row> assemblerFeature(Dataset<Row> input, String[] columns) {
    VectorAssembler assembler = new VectorAssembler().setInputCols(columns).setOutputCol("features");
    Dataset<Row> output = assembler.transform(input);
    return output;
  }
}

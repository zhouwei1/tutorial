import java.util.HashMap;

public interface Constants {

  Integer TRAIN_DATA_FORMAT = 1;

  Integer TEST_DATA_FORMAT = 0;

  HashMap<String, String> TITLE_MAP = new HashMap<String, String>() {
    {
      put("Capt", "Officer");
      put("Col", "Officer");
      put("Major", "Officer");
      put("Dr", "Officer");
      put("Rev", "Officer");
      put("Don", "Royalty");
      put("Sir", "Royalty");
      put("the Countess", "Royalty");
      put("Dona", "Royalty");
      put("Mme", "Mrs");
      put("Ms", "Mrs");
      put("Mrs", "Mrs");
      put("Mlle", "Miss");
      put("Miss", "Miss");
      put("Mr", "Mr");
      put("Master", "Master");
      put("Jonkheer", "Master");
    }
  };

  String DEFAULT_TITLE = "Other";
}
